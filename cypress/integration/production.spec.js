context('Testing dados.uff.br', function(){
  
  beforeEach(function(){
    cy.visit('http://dados.uff.br/')
  })

  describe('Testing links to datasets', function(){

    it('Cursos de Pós-Graduação - Dataset', function(){  
      cy.contains('Conjuntos de dados').click()
      cy.contains('Cursos de Pós-Graduação').click()
    })

    it('Técnicos-Administrativos - Dataset',function(){
      cy.contains('Conjuntos de dados').click()
      cy.contains('Técnicos-Administrativos').click()
    })

  })

})